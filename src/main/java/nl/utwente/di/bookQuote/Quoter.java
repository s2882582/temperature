package nl.utwente.di.bookQuote;

import java.util.Objects;

public class Quoter {
    public double getBookPrice(String isbn){
        if (Objects.equals(isbn, "1")){
            return 10.0;
        } else if (Objects.equals(isbn, "2")){
            return 45.0;
        } else if (Objects.equals(isbn, "3")){
            return 20.0;
        } else if (Objects.equals(isbn, "4")){
            return 35.0;
        } else if (Objects.equals(isbn, "5")){
            return 50.0;
        } else {
            return 0;
        }
    }
}
